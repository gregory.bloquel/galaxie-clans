# Commands dependencies
# =====================
#
DIRENV_CMD_DEPENDENCIES="unzip tar mkdir curl chmod rm"
for mandatory_cmd in ${DIRENV_CMD_DEPENDENCIES}; do
    if [ -z "$(which ${mandatory_cmd})" ]; then
         echo "--- mandatory command not found: ${mandatory_cmd}"
         exit 1
    fi
done
#
# Direnv configuration
# =====================
#
layout_python3
export DIRENV_TMP_DIR="${PWD}/.direnv"
export DIRENV_BIN_DIR="${DIRENV_TMP_DIR}/bin"
if [ ! -e "${DIRENV_BIN_DIR}" ]; then
    mkdir -p "${DIRENV_BIN_DIR}"
fi
export PATH="${DIRENV_BIN_DIR}:${PATH}"
#
# Ansible configuration
# =====================
#
export ANSIBLE_ROLES_PATH="${PWD}/roles"
export ANSIBLE_CALLBACKS_ENABLED="timer,profile_tasks"
export MOLECULE_COMMONS_DIR="${PWD}/molecule/_commons"
export MOLECULE_DOCKER_SSH_PORT_DEBIAN="22001"
export MOLECULE_DOCKER_SSH_PORT_ROCKY="22002"
export MOLECULE_DOCKER_SSH_PORT_ORACLE_9="22003"
export ANSIBLE_COLLECTIONS_PATH="${DIRENV_TMP_DIR}"
if [ -z "${ANSIBLE_LIBRARY}"]; then
    export ANSIBLE_LIBRARY="${PROJECT_DIR}/plugins/modules"
else
    export ANSIBLE_LIBRARY="${PROJECT_DIR}/plugins/modules:${ANSIBLE_LIBRARY}"
fi
#
# Sphinx configuration
# =====================
#
DIRENV_PYTHON_LIBS_DIR_RELATIVE="$(find ${DIRENV_TMP_DIR} -type d -name site-packages)"
export DIRENV_PYTHON_LIBS_DIR="$(realpath ${DIRENV_PYTHON_LIBS_DIR_RELATIVE}):$(realpath ${PWD}/plugins/)"
export PYTHONPATH="${DIRENV_PYTHON_LIBS_DIR}"
#
# Environment addons sourcing
# ===========================
#
SUPPORTED_ENV_ADDON_FILES=".env.scw .env.gandi .env.ansible"
for env_addon_file in ${SUPPORTED_ENV_ADDON_FILES}; do
    echo "+ loading ${PWD}/${env_addon_file}"
    if [ -e "${PWD}/${env_addon_file}" ]; then
        source ${PWD}/${env_addon_file}
    fi
done
#
# Terraform CLI installation
# ==========================
#
TF_VERSION="1.0.0"
TF_ARCH="linux_amd64"
TF_PKG_NAME="terraform_${TF_VERSION}_${TF_ARCH}.zip"
TF_PKG_URL="https://releases.hashicorp.com/terraform/${TF_VERSION}/${TF_PKG_NAME}"
TF_PKG_PATH="${DIRENV_TMP_DIR}/${TF_PKG_NAME}"
if [ ! -e "${DIRENV_BIN_DIR}/terraform" ]; then
    echo "===> Getting terraform:${TF_VERSION}:${TF_ARCH} (can take a while to execute)"
    curl -s -L "${TF_PKG_URL}" -o "${TF_PKG_PATH}"
    unzip ${TF_PKG_PATH} -d ${DIRENV_BIN_DIR}
    chmod 700 ${DIRENV_BIN_DIR}/terraform
    rm -f ${TF_PKG_PATH}
fi
#
# Terraform CLI configuration
# ==========================
#
export TF_PLUGIN_CACHE_DIR="${DIRENV_TMP_DIR}/terraform-plugin-cache"
if [ ! -e "${TF_PLUGIN_CACHE_DIR}" ]; then
    mkdir -p "${TF_PLUGIN_CACHE_DIR}"
fi
