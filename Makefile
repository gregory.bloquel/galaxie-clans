separator = "********************************************************************************"

.PHONY: header
header:
	@echo "***************************** GALAXIE CLANS MAKEFILE ***************************"
	@echo "HOSTNAME        `uname -n`"
	@echo "KERNEL RELEASE  `uname -r`"
	@echo "KERNEL VERSION  `uname -v`"
	@echo "PROCESSOR       `uname -m`"
	@echo $(separator)

.PHONY: help
help:
	@echo $(separator)
	@echo "`tput bold``tput smul`Galaxie Clans Ansible Toolkit`tput rmul``tput sgr0` v0.1"
	@echo $(separator)
	@echo ""
	@echo "  make prepare-debian"
	@echo "    => $(prepare-debian-desc)"
	@echo ""
	@echo "  make env"
	@echo "    => $(env-desc)"
	@echo ""
	@echo "  make clean-env"
	@echo "    => $(clean-env-desc)"
	@echo ""
	@echo "  make doc"
	@echo "    => $(doc-desc)"
	@echo ""
	@echo "  make clean-doc"
	@echo "    => $(clean-doc-desc)"
	@echo $(separator)

.PHONY: prepare-debian
prepare-debian-desc = "Prepare a Debian-based linux system for project operations"
prepare_debian:
	@echo ""
	@echo $(prepare-debian-desc)
	@echo $(separator)
	@sudo apt-get install direnv python3 python3-venv sshpass xsel xclip

.PHONY: env
env-desc = "Build local workspace environment"
prepare: header
	@echo ""
	@echo $(env-desc)
	@echo $(separator)
	@pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  OK  ] PIP3" || \
	echo "[FAILED] PIP3"

	@pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  OK  ] WHEEL" || \
	echo "[FAILED] WHEEL"

	@pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  OK  ] SETUPTOOLS" || \
	echo "[FAILED] SETUPTOOLS"

	@pip3 install -U --no-cache-dir --quiet -r ${PWD}/requirements.txt &&\
	echo "[  OK  ] REQUIREMENTS" || \
	echo "[FAILED] REQUIREMENTS"

	@pip3 install -U --no-cache-dir --quiet -r ${PWD}/docs/requirements.txt &&\
	echo "[  OK  ] DOC REQUIREMENTS" || \
	echo "[FAILED] DOC REQUIREMENTS"

	@pip3 install -U --no-cache-dir --quiet ${PWD}/docs/plugins/yaml2md/. &&\
	echo "[  OK  ] YAML2MD" || \
	echo "[FAILED] YAML2MD"

	@pip3 install -U --no-cache-dir --quiet -e  ${PWD}/ui/clanskeeper/. &&\
	echo "[  OK  ] CLANSKEEPER" || \
	echo "[FAILED] CLANSKEEPER"

	@ echo ""
	@ echo "************************ IMPORT EXTERNAL ANSIBLE ROLES ************************"
	ansible-galaxy collection install -fr requirements.yml

.PHONY: clean-env-desc
clean-env-desc = "Clean all project dependencies off the workspace"
clean-env:
	@echo ""
	@echo $(clean-env-desc)
	@echo $(separator)
	@rm -rf ${PWD}/.direnv

.PHONY: doc-desc
doc-desc = "Build project static html documentation"
doc:
	@echo ""
	@echo $(doc-desc)
	@echo $(separator)
	@cd docs &&	make html
	@echo $(separator)
	@echo "Static documentation exported:"
	@echo "  file://${PWD}/docs/build/html/index.html"
	@echo $(separator)

.PHONY: clean-doc-desc
clean-doc-desc = "Clean project static html documentation"
clean-doc:
	@echo ""
	@echo $(clean-doc-desc)
	@echo $(separator)
	@cd docs && make clean

.PHONY: ide
ide-desc = "Install galaxie-clans IDE for your local user"
ide:
	@echo ""
	@echo $(clean-doc-desc)
	@echo $(separator)
	@ansible-playbook playbooks/install_ide.yml -e scope=localhost -e neovim_user=`whoami` -K

# TODO: ansible-collection release target

.PHONY: clan-create
clan-create-desc = "Create a clan"
clan-create:
	@echo ""
	@echo $(clan-create-desc)
	@echo $(separator)
	[ -n "${CLAN_WORKSPACE}" ] && \
	ansible-playbook playbooks/create_cloud_host.yml -e workspace=${CLAN_WORKSPACE} && \
	ansible-playbook playbooks/02_core.yml -e scope=${CLAN_WORKSPACE}

#	ansible-playbook playbooks/gandi_delegate_subdomain.yml -e scope=${CLAN_WORKSPACE} -e mode=destroy -e force=true && \
#	ansible-playbook playbooks/gandi_delegate_subdomain.yml -e scope=${CLAN_WORKSPACE} && \
#	ansible-playbook playbooks/acme_rotate_certificates.yml -e scope=${CLAN_WORKSPACE}

#	ansible-playbook playbooks/03_broadcast.yml -e scope=${CLAN_WORKSPACE}

.PHONY: clan-destroy
clan-destroy-desc = "Detroy a clan"
clan-destroy:
	@echo ""
	@echo $(clan-destroy-desc)
	@echo $(separator)
	[ -n "${CLAN_WORKSPACE}" ] && \
	ansible-playbook playbooks/gandi_delegate_subdomain.yml -e scope=${CLAN_WORKSPACE} -e mode=destroy -e force=true && \
	ansible-playbook playbooks/delete_cloud_host.yml -e workspace=${CLAN_WORKSPACE}
