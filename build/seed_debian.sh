#!/usr/bin/env bash

if [ -d "/etc/ansible/facts.d" ] && [ "$(cat /etc/ansible/facts.d/seed.fact)" = "{\"planted\": true}" ]; then
    #exit 42
    echo "detected"
fi

export DEBIAN_FRONTEND=noninteractive

if [ -n "$(dpkg -l | grep exim)" ]; then
    apt-get -o Dpkg::Options::="--force-confold" install postfix -y
    apt-get -o Dpkg::Options::="--force-confold" remove exim4 exim4-base exim4-config exim4-daemon-light --purge -y
fi

if [ -n "$(dpkg -l | grep python-pip)" ]; then
    apt-get -o Dpkg::Options::="--force-confold" remove python-pip --purge -y
fi

apt-get update
apt-get -o Dpkg::Options::="--force-confold" upgrade -y
apt-get update

apt-get -o Dpkg::Options::="--force-confold" install \
    sudo                \
    aptitude            \
    git                 \
    python3-apt          \
    apt-transport-https \
    python3-distutils-extra \
    python3-setuptools   \
    python3-apt-dev      \
    python3-dev          \
    libssl-dev          \
    libffi-dev          \
    build-essential     \
    libperl-dev         \
    -y

pip3 install -U pexpect ansible

mkdir -p /etc/ansible/facts.d
echo "{\"planted\": true}" > /etc/ansible/facts.d/seed.fact

#git clone https://gitlab.com/Tuuux/galaxie-clans.git
#cd galaxie-clans 
ansible-playbook playbooks/ftx.yml
