# Configuration file for the Sphinx documentation builder.
#
# Reference documentation:
#   https://www.sphinx-doc.org/en/master/usage/configuration.html
#
# -- Project information -----------------------------------------------------
#
project = "galaxie_clans"
copyright = "2023, R.T.N.P"
author = "R.T.N.P"
release = "v1.13"
#
# -- General configuration ---------------------------------------------------
#
extensions = [
    "myst_parser",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autodoc",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx_copybutton"
]

copybutton_selector = "div:not(.no-copybutton) > div.highlight > pre"
copybutton_prompt_text = "> "

templates_path = ['_templates']
exclude_patterns = []
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}
#
# -- Options for HTML output -------------------------------------------------
#
html_theme = "furo"
html_logo = "_static/img/logo_debian_rtnp_galaxie6-256.png"
html_static_path = ["_static"]
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']
#
# -- Ansible role inline doc extraction --------------------------------------
#
import os, sys, yaml2md

roles_src_path = os.getenv("ANSIBLE_ROLES_PATH")
ignore_role_list = ['galaxie_clans.dns', 
    'galaxie_clans.clan_host',
    'galaxie_clans.system_base',
    'galaxie_clans.system_users'
]
if not roles_src_path:
    roles_src_path = "../../roles"
roles_doc_path = "reference/role"

for element in os.listdir(roles_src_path):
    if not os.path.isdir(roles_src_path + "/" + element + "/defaults") or element in ignore_role_list:
        continue
    os_walk = os.walk(roles_src_path + "/" + element + "/defaults")
    for path, subdirs, files in os_walk:
        for filename in files:
            if filename.startswith("."):
                continue

            defaults_file = os.path.join(path, filename)
            defaults_dir = roles_doc_path + "_" + element

            yaml2md.convert_file(
                defaults_file,
                roles_doc_path + "_" + element + ".md",
                strip_regex=r"\s*(:?\[{3}|\]{3})\d?$",
                yaml_strip_regex=r"^\s{66,67}#\s\]{3}\d?$",
            )
            print("Converted {0}".format(defaults_dir))

