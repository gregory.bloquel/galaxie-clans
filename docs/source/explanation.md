# Explanations

```{toctree}
:maxdepth: 1

explanation/once_upon
explanation/license
explanation/code_of_conduct
explanation/testing_ansible_roles
explanation/galaxie_ide.rst
explanation/links.rst
```
