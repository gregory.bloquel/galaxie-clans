###############
Roles Testing
###############

*************************************
Molecule integration in Galaxie-clans
*************************************

Tests locations
===============

Role-specific testing code
--------------------------

The Molecule configuration and testing playbooks are located in each role subdirectory:

.. code::

    [...]
    └── system_users
        ├── molecule
        │   └── default
        │       ├── converge.yml    # <--- Playbook to apply and check for idempotence
        │       ├── molecule.yml    # <--- Scenario configuration
        │       └── verify.yml      # <--- Assertion-stuffed validation playbook
    [...]

Common testing code
--------------------------

The role-specific ``molecule.yml`` should point to a common ``prepare.yml`` file. Its goal is to
setup the test container to react as much as possible like a true system host.

.. code::

    [...]
    ├── test
    │   ├── molecule
    │   │   ├── prepare_debian.yml  # <--- Debian-specific preparation playbook
    │   │   └── prepare.yml         # <--- Common preparation playbook
    [...]

Launching tests
===============

* Place your terminal in a role directory. Let's say ``roles/system_base`` for the example.
* Launch Molecule

.. code:: bash

    cd roles/system_base
    molecule test

If you want to play on testing state, have a look at the
`structure of a Molecule Scenario <https://molecule.readthedocs.io/en/latest/configuration.html#molecule.scenario.Scenario>`_.



Links
=====

* `Getting Started with Molecule <https://molecule.readthedocs.io/en/latest/getting-started.html>`_
* `Molecule Docker Driver Reference <https://github.com/ansible-community/molecule-docker/blob/master/molecule_docker/driver.py>`_
