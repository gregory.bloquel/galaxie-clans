# How-to guides

```{toctree}
:maxdepth: 1

howto/get_started
howto/use_as_dependency
howto/use_as_project
howto/add_server
howto/init_host
howto/contribute
```

## Off-topic

```{toctree}
:maxdepth: 1

howto/offline_bcm
```