# Host init

Any host is delivered with an __initial access__ that can differ from one another.

To avoid problems, galaxie-clans' strategy is to begin with configuring a service user.

You will need:

* The initial access `username`.
* The initial access `password` OR associated SSH private key.

## IF initial access is NOT root

Check the following envrionment variables and determine what suits your case:

* [ANSIBLE_BECOME_METHOD](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#envvar-ANSIBLE_BECOME_METHOD)
* [ANSIBLE_BECOME_ASK_PASS](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#envvar-ANSIBLE_BECOME_ASK_PASS)

## Init host

```{code-block}
> ANSIBLE_BECOME_ASK_PASS="[false|true]" \
ANSIBLE_BECOME_METHOD="[sudo|su]" \
GLXCLANS_INIT_HOSTNAME="naming" \
GLXCLANS_INIT_HOST="172.16.1.31" \
GLXCLANS_INIT_USER="root" \
GLXCLANS_INIT_IDENTITY_FILE="/path/to/private/key" \
[GLXCLANS_INIT_SSH_CONFIG_FILE=./ssh.cfg] \
[GLXCLANS_INIT_INVENTORYFILE=./inventory] \
ansible-playbook rtnp.galaxie_clans.init_host
```

## Review



```{code-block}
:caption:
:linenos:

ANSIBLE_VAULT_PASSWORD_FILE et fichier existe et non-vide => coup de ansible-vault 
après génération du fichier yaml contenant le password sur service user
```

## Adopt host


