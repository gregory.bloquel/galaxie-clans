********************************************
Install Broadcom drivers on a offline Debian
********************************************

Certainly off-topic but interesting as hell when you are in this
specific kind of trouble.

The Scene
=========

During a fresh install of a Debian Testing on a Lenovo S20-30:

* used this `Debian CD image <https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-live-builds/amd64/iso-hybrid/>`_
  with nonfree firmwares, genuinely thinking it would do the job.
* Clean install but no Wifi... 

Pretty hard to go on and finish the setup.

.. code:: bash

    $ lspci
    [...]
    02:00.0 Network controller: Broadcom Inc. and subsidiaries BCM43142 802.11b/g/n (rev 01)

The Solution
============

You will need:

* another computer with Internet connectivity
* a USB key

Use `pkgs.org <https://pkgs.org/>`_ to track and download (in this case for Debian Sid):

* dctrl-tools
* dkms
* broadcom-sta-common
* broadcom-sta-dkms

Put these packages on the USB key, transfer to your offline computer and install them by running (as ``root``):

.. code:: bash

    dpkg -i dctrl-tools_2.24-3+b1_amd64.deb
    dpkg -i dkms_2.8.4-4_all.deb
    dpkg -i broadcom-sta-common_6.30.223.271-17_all.deb
    dpkg -i broadcom-sta-dkms_6.30.223.271-17_all.deb
    modprobe -r b44 b43 b43legacy ssb brcmsmac bcma
    modprobe wl

.. warning::

    These are the package names corresponding to my architecture and current version at the redaction 
    date. You need to adapt to your space-time and this might become outdated.

Enjoy.