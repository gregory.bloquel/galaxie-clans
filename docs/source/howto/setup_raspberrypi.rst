
https://www.pragmaticlinux.com/2020/06/setup-your-raspberry-pi-4-as-a-headless-server/

ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=<Insert 2 letter ISO 3166-1 country code here>
​
network={
 ssid="<Name of your wireless LAN>"
 psk="<Password for your wireless LAN>"
}
