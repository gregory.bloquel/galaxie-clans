*****************************************
Use galaxie-clans as a project
*****************************************

You can use this project as a well stuffed, ready to use, Ansible workspace.

* Follow the How-to :doc:`/howto/get_started` Guide
* Follow the How-to :doc:`/howto/add_server` Guide
* Play around