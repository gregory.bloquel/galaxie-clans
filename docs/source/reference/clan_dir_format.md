# Clan directory management

A __clan__, aka a group of hosts managed by the rtnp.galaxie_clans ansible collection,
is defined by:

* a dedicated directory containing...
* ...an `ansible.cfg` file
* ...an `ssh.cfg` file
* ...an `inventory` file

## Conventions

* All of the clan hosts are members of the `[glxclans]` inventory group.
* All of the clan group vars are written in yaml format under `group_vars/glxclans/` directory.
* All managed host must have a short name ([RFC-1035](https://www.rfc-editor.org/rfc/rfc1035) compliant).
* All ssh connection parameters for a host must be in the `ssh.cfg` file.
* The short name of a host must match between `inventory` and `ssh.cfg`.
* No additional ansible vars should be declared in the `inventory` file.
* Any specific clan host var is written in yaml format under `host_vars/${inventory_hostname}/whatever.yml`.
* For all hosts, facts caching must be enabled. TOM is to make it unitary available to read for any
upper-level binary.