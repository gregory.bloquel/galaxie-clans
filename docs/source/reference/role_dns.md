
```{include} ../../../roles/dns/README.md
```

glxclans_dns_enable: false

glxclans_dns_domain: "{{ glxclans_system_base_domain }}"

glxclans_dns_views:
  - name: default
    match_clients:
      - "any"

glxclans_dns_zone_domains:
  - name: "example.com"
    hostmaster_email: "hostmaster"
    networks:
      - "10.0.2"
```
List of acls.
```

glxclans_dns_acls: []

```
Key binding for slaves
```

glxclans_dns_keys: []
```
 - name: master_key
   algorithm: hmac-sha256
   secret: "azertyAZERTY123456"

```

glxclans_dns_tsig_keys: []
```
 - name: "{{ public_domain }}."
   algorithm: hmac-sha256

List of IPv4 address of the network interface(s) to listen on. Set to "any"
to listen on all interfaces
```

glxclans_dns_listen_ipv4:
  - "127.0.0.1"

```
List of IPv6 address of the network interface(s) to listen on.
```

glxclans_dns_listen_ipv6:
  - "::1"

```
List of hosts that are allowed to query this DNS server.
```

glxclans_dns_allow_query:
  - "localhost"

```
Determines whether recursion should be allowed. Typically, an authoritative
name server should have recursion turned OFF.
```

glxclans_dns_recursion: false
glxclans_dns_allow_recursion:
  - "any"

```
Allows BIND to be set up as a caching name server
```

glxclans_dns_forward_only: false

```
List of name servers to forward DNS requests to.
```

glxclans_dns_forwarders: []

```
DNS round robin order (random or cyclic)
```

glxclans_dns_rrset_order: "random"

```
statistics channels configuration
```

glxclans_dns_statistics_channels: false
glxclans_dns_statistics_port: 8053
glxclans_dns_statistics_host: 127.0.0.1
glxclans_dns_statistics_allow:
  - "127.0.0.1"

```
DNSSEC configuration
```

glxclans_dns_dnssec_enable: true
glxclans_dns_dnssec_validation: true

glxclans_dns_extra_include_files: []

```
SOA information
```

glxclans_dns_zone_ttl: "1W"
glxclans_dns_zone_time_to_refresh: "1D"
glxclans_dns_zone_time_to_retry: "1H"
glxclans_dns_zone_time_to_expire: "1W"
glxclans_dns_zone_minimum_ttl: "1D"

```
Custom location for master zone files

File mode for master zone files (needs to be something like 0660 for dynamic updates)
```

glxclans_dns_zone_file_mode: "0640"

glxclans_dns_user_keys_algorithm: HMAC-SHA512
glxclans_dns_user_keys_size: 512
