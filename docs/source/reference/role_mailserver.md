
```{include} ../../../roles/mailserver/README.md
```

glxclans_mailserver_enable: no

glxclans_mailserver_hostname: "{{ inventory_hostname | replace('_', '-') }}"
glxclans_mailserver_base_domain: "{{ glxclans_system_base_domain }}"
```

System user owning the virtual mailboxes

```

glxclans_mailserver_mta_virtual_mailbox_gname: vmail
glxclans_mailserver_mta_virtual_mailbox_gid: 5000
glxclans_mailserver_mta_virtual_mailbox_uname: vmail
glxclans_mailserver_mta_virtual_mailbox_uid: 5000
glxclans_mailserver_mta_virtual_mailbox_home: /home/vmail
glxclans_mailserver_mta_virtual_mailbox_shell: /usr/bin/nologin

```

K/V content

```

glxclans_mailserver_postfix_transports: []
```
- domain: "example.com"
  transport: "smtp:internal.example.com"
```

glxclans_mailserver_virtual_aliases: []
```
- alias: "info-alias@example.com"
  email: "info@example.com"
```

glxclans_mailserver_virtual_mailboxes:
  - email: "hostmaster@{{ glxclans_mailserver_base_domain }}"
    recipient: "hostmaster@{{ glxclans_mailserver_base_domain }}/"
    system_user: autobot
```
- email: "info@example.com"
  recipient: "example.com/info"
```

glxclans_mailserver_virtual_mailboxes_domains:
  - "{{ glxclans_mailserver_base_domain }}"

glxclans_mailserver_mta_trust_cidr:
  - "127.0.0.1/32"
```
- 192.168.0.0/16
- 10.42.42.0/24

```

glxclans_mailserver_spam_report_address: ~
glxclans_mailserver_opendkim_key_size: 1024

glxclans_mailserver_smtpd_tls_cert_file: >-
  {{ __glxclans_mailserver_certs_private_store_dir }}/{{ __glxclans_mailserver_fqdn }}.fullchain.crt
glxclans_mailserver_smtpd_tls_key_file: >-
  {{ __glxclans_mailserver_certs_private_store_dir }}/{{ __glxclans_mailserver_fqdn }}.key

glxclans_mailserver_smtpd_sasl_local_domain: "localhost"
glxclans_mailserver_mta_virtual_uid_maps: "static:{{ glxclans_mailserver_mta_virtual_mailbox_uid }}"
glxclans_mailserver_mta_virtual_gid_maps: "static:{{ glxclans_mailserver_mta_virtual_mailbox_gid }}"
glxclans_mailserver_mta_virtual_minimum_uid: "{{ glxclans_mailserver_mta_virtual_mailbox_uid }}"
glxclans_mailserver_mta_virtual_mailbox_limit: "512000000"
glxclans_mailserver_mta_max_queue_lifetime: "5d"
glxclans_mailserver_mta_min_backoff_time: "300s"
glxclans_mailserver_mta_max_backoff_time: "4000s"

glxclans_mailserver_virtual_aliases_domains: "{{ glxclans_mailserver_virtual_mailboxes_domains }}"
