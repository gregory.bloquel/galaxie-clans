
```{include} ../../../roles/rproxy/README.md
```

## Defaults

```

glxclans_rproxy_enable: false

glxclans_rproxy_nginx_stream_conf_dir: "/etc/nginx/streams.conf.d"
glxclans_rproxy_nginx_managed_sites: []
glxclans_rproxy_nginx_managed_streams: []

glxclans_rproxy_nginx_enable_sites: false
glxclans_rproxy_nginx_enable_streams: false
