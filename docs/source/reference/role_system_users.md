
```{include} ../../../roles/system_users/README.md
```

## Defaults

You can fill this list with the users groups you want to be created.

```

glxclans_system_users_groups: []
```

Expected structure of a group definition is:

```yaml
  - gname: "hurd"    # (mandatory)
    gid: 42001       # (optional)
```

----

You can fill this list with the users you want to be created.

A group will be created with the same name as the ``uname`` attribute and
the user will be set as member.


```

glxclans_system_users_secrets_dir: "{{ glxclans_group_secrets_dir }}"
glxclans_system_users:
  - uname: autobot
    uid: 47000
    shell: /bin/false
```

Expected structure of a user definition is:

```yaml

  - uname: "zorro"                       # (mandatory)
    uid: "42001"                         # (optional)
    groups:                              # (optional)
      - "sudo"
    home: "/home/zorro"                  # (optional)
    shell: "/bin/bash"                   # (optional)
    authorized_key: "ssh-ed25519 ..."    # (optional)
```

