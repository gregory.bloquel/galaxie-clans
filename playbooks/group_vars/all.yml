---
ansible_pwd: "{{ lookup('env', 'PWD') }}"

gandi_api_key: "{{ lookup('env', 'GANDI_API_KEY') }}"
gandi_domain: "{{ lookup('env', 'GANDI_DOMAIN') }}"
gandi_subdomain: "{{ lookup('env', 'GANDI_SUBDOMAIN') }}"
gandi_subdomain_ns: "ns.{{ gandi_subdomain }}"

#########################################
# GENERAL
#
glxclans_dns_views:
  - name: default
    match_clients:
      - "any"

# Used as default in many roles, should be defined
glxclans_system_base_domain: "{{ ansible_hostname | default('clans.galaxie.family') }}"

glxclans_workspace: "{{ inventory_dir }}"
glxclans_group_vars_dir: "{{ glxclans_workspace }}/group_vars/glxclans"
glxclans_group_secrets_dir: "{{ glxclans_group_vars_dir }}/secrets"

local_secrets_dir: "{{ (playbook_dir + '/../secrets') | realpath }}"
local_keys_dir: "{{ (playbook_dir + '/../keys') | realpath }}"
local_backups_dir: "{{ (playbook_dir + '/../backups') | realpath }}"
ssl_certs_dir: "/etc/ssl/private"
caretaker_authorized_key_files:
  - "{{ (playbook_dir + '/../keys/' + inventory_hostname + '.key.pub') | realpath }}"

ssl_certs_file_extensions:
  - "crt"
  - "fullchain.crt"
  - "key"
#########################################
# PLAYBOOK: time_machine_pull.yml + time_machine_push.yml
#
time_machine_backup_dir: "{{ local_backups_dir }}/{{ inventory_hostname }}"
time_machine_backup_profiles: >-
  {{
    vars.ansible_local.keys()
    | select('match', '^backup_profile_.*$')
    | list
  }}
time_machine_date_marker: "{{ ansible_date_time.iso8601 }}"
time_machine_date_marker_regex: >-
  (([1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])Z
time_machine_rsync_opts_common:
  - "--devices"
  - "--numeric-ids"
  - "--hard-links"
  - "--one-file-system"
  - "--itemize-changes"
  - "--stats"
  - "--human-readable"
  - "--fake-super"
  - "--xattrs"
  - "--acls"
time_machine_rsync_opts_push: "{{ time_machine_rsync_opts_common }}"
time_machine_link_dest_dir_prefix: >-
  {%- if (existing_backups | length == 0) -%}
  {{ time_machine_current_backup_dir }}
  {%- else -%}
  {{ time_machine_backup_dir }}/{{ existing_backups[0] }}
  {%- endif -%}
time_machine_rsync_opts_link_dest: >-
  --link-dest={{ time_machine_link_dest_dir_prefix }}/{{ item[0].key }}/{{ item[1].name }}
time_machine_rsync_opts_pull: >-
  {{ time_machine_rsync_opts_common + [time_machine_rsync_opts_link_dest] }}
#########################################
# ROLE: dns
#
glxclans_dns_allow_query: ['any']
glxclans_dns_listen_ipv4:
  - "{{ ansible_default_ipv4.address }}"
  - "127.0.0.1"
glxclans_dns_listen_ipv6:
  - "{{ ansible_default_ipv6.address }}"
  - "::1"
glxclans_dns_recursion: true
glxclans_dns_allow_recursion:
  - "127.0.0.1"
glxclans_dns_forwarders:
  - "127.0.0.1"
glxclans_dns_zones:
  - name: "{{ glxclans_system_base_domain }}"
    type: master
    hostmaster_email: "hostmaster.{{ glxclans_system_base_domain }}"
    allow_update:
      - "localhost"
    name_servers:
      - ns1
    mail_servers:
      - name: mail
        preference: 10
    text:
      - name: "{{ glxclans_system_base_domain }}."
        text: "v=spf1 mx -all"
    hosts:
      - name: ""
        ip: "{{ ansible_default_ipv4.address }}"
        ipv6: "{{ ansible_default_ipv6.address }}"

      - name: "{{ inventory_hostname }}"
        ip: "{{ ansible_default_ipv4.address }}"
        ipv6: "{{ ansible_default_ipv6.address }}"
        aliases:
          - cal
          - chat
          - meet
          - stun

      - name: ns1
        ip: "{{ ansible_default_ipv4.address }}"
        ipv6: "{{ ansible_default_ipv6.address }}"

      - name: mail
        ip: "{{ ansible_default_ipv4.address }}"
        ipv6: "{{ ansible_default_ipv6.address }}"

#########################################
# ROLE: mailserver
#

mailserver_spam_report_contact: "hostmaster@{{ glxclans_system_base_domain }}"

#########################################
# ROLE: videoconf
#
jitsi_meet_public_domain: "meet.{{ glxclans_system_base_domain }}"
jitsi_meet_stun_public_domain: "stun.{{ glxclans_system_base_domain }}"
jitsi_meet_support_url: "https://{{ jitsi_meet_public_domain }}"
jitsi_meet_certificate_file: "{{ ssl_certs_dir }}/{{ glxclans_system_base_domain }}.fullchain.crt"
jitsi_meet_certificate_key_file: "{{ ssl_certs_dir }}/{{ glxclans_system_base_domain }}.key"
jitsi_meet_stun_servers:
  - "stun:{{ jitsi_meet_stun_public_domain }}:3478"
  - "stun:stun.galaxie.eu.org:3478"
  - "stun:stun.clans.galaxie.family:3478"

#########################################
# ROLE: rproxy
#
glxclans_rproxy_nginx_managed_sites:
  - "jitsi-meet"
  - "calendar"
  - "mattermost"

#########################################
# PLAYBOOK: acme_rotate_certificates.yml
#
acme_account_email: "hostmaster@{{ glxclans_system_base_domain }}"
acme_domains:
  - cn: "{{ glxclans_system_base_domain }}"
    zone: "{{ glxclans_system_base_domain }}"
    alt:
      - "DNS:mail.{{ glxclans_system_base_domain }}"
      - "DNS:{{ calendar_public_domain }}"
      - "DNS:{{ mattermost_public_domain }}"
      - "DNS:meet.{{ glxclans_system_base_domain }}"
      - "DNS:stun.{{ glxclans_system_base_domain }}"
      - "DNS:{{ inventory_hostname }}.{{ glxclans_system_base_domain }}"
acme_nsupdate_host: "127.0.0.1"
acme_nsupdate_delegate: ~
acme_nsupdate_become: true

#########################################
# ROLE: calendar
#
calendar_public_domain: "cal.{{ glxclans_system_base_domain }}"
calendar_certificate_file: "{{ ssl_certs_dir }}/{{ glxclans_system_base_domain }}.fullchain.crt"
calendar_certificate_key_file: "{{ ssl_certs_dir }}/{{ glxclans_system_base_domain }}.key"

#########################################
# ROLE: chat
#

mattermost_public_domain: "chat.{{ glxclans_system_base_domain }}"

mattermost_certificate_file: "{{ ssl_certs_dir }}/{{ glxclans_system_base_domain }}.fullchain.crt"
mattermost_certificate_key_file: "{{ ssl_certs_dir }}/{{ glxclans_system_base_domain }}.key"

mattermost_sitename: "Chat"
mattermost_jitsi_meet_url: "https://{{ jitsi_meet_public_domain }}"

mattermost_email_feedback: "hostmaster@{{ glxclans_system_base_domain }}"
mattermost_support_email: "{{ mattermost_email_feedback }}"
mattermost_email_reply_to: "{{ mattermost_email_feedback }}"

mattermost_smtp_host: "mail.{{ glxclans_system_base_domain }}"
mattermost_smtp_port: "465"
mattermost_smtp_mail_system_user: "{{ (system_users | selectattr('uid', 'equalto', 47000) | list)[0].uname }}"
mattermost_smtp_user: >-
  {{ mattermost_smtp_mail_system_user }}@{{ glxclans_system_base_domain }}
mattermost_smtp_pass: >-
  {{ 
    lookup('password', 
      local_secrets_dir + '/system_users/' + inventory_hostname + '/' + mattermost_smtp_mail_system_user + '.password'
    )
  }}
mattermost_smtp_connection_security: "TLS"
