---
- name: Init current directory as a galaxie-clans workspace
  hosts: localhost
  gather_facts: true
  gather_subset:
    - date_time
  become: false

  vars:
    glxclans_name: "{{ lookup('env', 'GLXCLANS_NAME') }}"
    glxclans_dir: "{{ ansible_pwd }}/{{ glxclans_name }}"
    glxclans_oath: "By the will of the Operator the clan '{{ glxclans_name }}' now IS."
    glxclans_roles_dir: "{{ (playbook_dir + '/../roles') | realpath }}"

  tasks:
    - name: Assert mandatory values
      assert:
        that:
          - glxclans_name is defined
          - (glxclans_name | length) >= 3
        msg: >-
          Variable 'glxclans_name' must be defined and have length >= 3. DO
          append '-e glxclans_name=[...]' OR define env var 'GLXCLANS_NAME'.

    - name: Create clan directory
      file:
        path: "{{ _current_dir }}"
        state: directory
      loop:
        - "{{ glxclans_dir }}"
        - "{{ glxclans_dir }}/group_vars"
        - "{{ glxclans_dir }}/group_vars/glxclans"
      loop_control:
        loop_var: _current_dir

    - name: Git init clan dir
      command: >-
        git init
      args:
        chdir: "{{ glxclans_dir }}"
        creates: "{{ glxclans_dir }}/.git"

    - name: Render ansible.cfg
      template:
        src: "ansible.cfg.j2"
        dest: "{{ glxclans_dir }}/ansible.cfg"
      notify: Git commit

    - name: Ensure ssh.cfg existence
      blockinfile:
        path: "{{ _current_conf_file }}"
        marker: "# {mark} playbook rtnp.galaxie_clans.init_clan"
        create: true
        block: |-
          # {{ glxclans_oath }}
      loop:
        - "{{ glxclans_dir }}/ssh.cfg"
        - "{{ glxclans_dir }}/inventory"
      loop_control:
        loop_var: _current_conf_file
      notify: Git commit

    - name: Ensure clan group_vars existence
      blockinfile:
        path: "{{ glxclans_dir }}/group_vars/glxclans/main.yml"
        marker: "# {mark} playbook rtnp.galaxie_clans.init_clan"
        create: true
        block: |-
          glxclans_name: "{{ glxclans_name }}"
      notify: Git commit

    - name: Enroll all roles' default variables
      copy:
        src: "{{ glxclans_roles_dir }}/{{ _current_role }}/defaults/main.yml"
        dest: "{{ glxclans_dir }}/group_vars/glxclans/role_{{ _current_role }}.yml"
        force: false
      loop:
        - host
        - system_base
        - system_users
        - dns
        - certs
        - mailserver
        - rproxy
        - monitor
      loop_control:
        loop_var: _current_role

  handlers:
    - name: Git commit
      shell: >-
        git add . && 
        git commit -m "{{ glxclans_oath }}"
      args:
        chdir: "{{ glxclans_dir }}"

