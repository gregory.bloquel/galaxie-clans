# certs

## Mode "acme"

* dealing with letsencrypt
* DNS challenge, operated from the controlled host that must be authority on the domain certified.

## Mode "ownca"

* You can supply a root CA certificate with its private key if none found, it will be generated.
* An intermediate CA will be generated
* The intermediate CA will be added to controlled hosts' trust store
* A leaf certificate will be generated for every host and made available for services to rely on.
