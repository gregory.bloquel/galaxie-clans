# mailserver

- Install and configure the Postfix service
- Install and configure the Dovecot service
- Integrate both services
- Configure user mailboxes
- Install and configure the SpamAssassin service
- Integrate ldap in postfix

## Role Variables


| Variable   | Default | Comments (type)  |
| :---       | :---    | :---             |
| `postfix_myhostname` | mail.bertvv.local      | The internet hostname of this mail systems |
| `postfix_mydomain` |  bertvv.local |  The internet domain name of this mail system. |
| `postfix_home_mailbox`  |  /mail |   Path name of the mailbox in the user's home directory|
| `postfix_ldap`  |  false |   To use or not to use ldap |
| `ldap_fqdn1`  |  / |   The FQDN of the ldap server, if `postfix_ldap` is true |
| `ldap_ou`  |  / |   The OU where the ldap users are created, if `postfix_ldap` is true |
| `ldap_dcname`  |  / |   The name of the ldap server, if `postfix_ldap` is true |
| `ldap_domainname`  |  / |   The domainname the network, if `postfix_ldap` is true |
| `ldap_root_domain`  |  / |   The root domain lever extension of the network, if `postfix_ldap` is true |

To creat a user and to create his mailbox:

```
rhbase_users:
  - name: johndoe
    password: '$6$WIFkXf07Kn3kALDp$fHbqRKztuufS895easdT [...]'
    shell: /sbin/nologin
```

This created user can login to the mailserver but not into the linux machine.